/**
 * 
 */
package snakesladdersgraphics;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author anuj
 *
 */
public class MainGameGraphic {

	/**
	 * @param args
	 */
	
	private JButton playGame;
	private JFrame mainFrame;
	private JPanel buttonPanel;
	
	public MainGameGraphic() {
		// TODO Auto-generated constructor stub
		mainFrame = new JFrame();
		mainFrame.setSize(900, 700);
		BoardGraphics g = new BoardGraphics();
		g.validate();
		mainFrame.add(g);
		playGame = new JButton("Start Game!!");
		LayoutManager layout = new FlowLayout();
		buttonPanel = new JPanel(layout);
		buttonPanel.add(playGame);
		buttonPanel.setBackground(Color.WHITE);
		buttonPanel.setBounds(800, 10, 70, 70);
		buttonPanel.validate();
		//mainFrame.add(buttonPanel);
		mainFrame.validate();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MainGameGraphic mgg = new MainGameGraphic();
	}

}
