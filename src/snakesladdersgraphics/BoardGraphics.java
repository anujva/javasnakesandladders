package snakesladdersgraphics;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class BoardGraphics extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		BufferedImage img = null;
		BufferedImage imgResized = null;
		try {
			img = ImageIO.read(new File("/home/anuj/workspace/spearsnakes.jpg"));
			imgResized = new BufferedImage(500, 500, img.getType());
			Graphics2D imgResizedGraphics = imgResized.createGraphics();
			imgResizedGraphics.drawImage(img, 0, 0, 500, 500, null);
			imgResizedGraphics.dispose();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g.drawImage(imgResized, 0, 0, null);
	}
	
	public static void main(String[] args) {
		JFrame jf = new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BoardGraphics g = new BoardGraphics();
		JButton throwDice = null;
		JButton startGame = null;
		throwDice = new JButton("Throw Dice");
		startGame = new JButton("Start game!!");
		g.add(throwDice);
		g.add(startGame);
		g.validate();
		jf.add(g);
		jf.setSize(800, 700);
		jf.setVisible(true);
	}
}
