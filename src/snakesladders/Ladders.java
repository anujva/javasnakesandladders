package snakesladders;

public class Ladders extends BoardJump{
	protected void setNode1AndNode2(int _node1, int _node2){
		try{
			if(_node1 > _node2){
				throw new IllegalLadderException();
			}
			super.setNode1AndNode2(_node1, _node2);
		}catch(IllegalLadderException e){
			e.print();
		}
	}
}
