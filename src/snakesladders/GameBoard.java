/**
 * 
 */
package snakesladders;

/**
 * @author Anuj_Varma
 *
 */
public final class GameBoard {

	/**
	 * @param args
	 */
	
	private int[] gameboard;
	
	Snakes[] snakes;
	Ladders[] ladders;
	
	public GameBoard() {
		// TODO Auto-generated constructor stub
		snakes = new Snakes[8];
		for(int i=0; i<snakes.length; i++){
			snakes[i] = new Snakes();
		}
		snakes[0].setNode1AndNode2(17, 13);
		snakes[1].setNode1AndNode2(88, 18);
		snakes[2].setNode1AndNode2(52, 29);
		snakes[3].setNode1AndNode2(97, 79);
		snakes[4].setNode1AndNode2(57, 40);
		snakes[5].setNode1AndNode2(62, 22);
		snakes[6].setNode1AndNode2(95, 51);
		snakes[7].setNode1AndNode2(27, 12);
		ladders = new Ladders[8];
		for(int i=0; i<ladders.length; i++){
			ladders[i] = new Ladders();
		}
		ladders[0].setNode1AndNode2(8, 30);
		ladders[1].setNode1AndNode2(5, 21);
		ladders[2].setNode1AndNode2(28, 84);
		ladders[3].setNode1AndNode2(58, 77);
		ladders[4].setNode1AndNode2(90, 91);
		ladders[5].setNode1AndNode2(80, 99);
		ladders[6].setNode1AndNode2(35, 42);
		ladders[7].setNode1AndNode2(10, 11);
		
		gameboard = new int[100];
		for(int i=0; i<gameboard.length; i++){
			gameboard[i] = i+1;
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
