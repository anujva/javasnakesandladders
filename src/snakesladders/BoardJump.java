/**
 * 
 */
package snakesladders;

/**
 * @author Anuj_Varma
 *
 */
public abstract class BoardJump {
	protected int node1;
	protected int node2;
	
	public BoardJump(){
		node1 = 0;
		node2 = 0;
	}
	
	public BoardJump(int _node1, int _node2){
		node1 = _node1;
		node2 = _node2;
	}
	
	protected void setNode1AndNode2(int _node1, int _node2){
		node1 = _node1;
		node2 = _node2;
	}
	
	protected int getNode1(){
		return node1;
	}
	
	protected int getNode2(){
		return node2;
	}
}
