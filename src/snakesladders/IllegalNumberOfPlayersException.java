package snakesladders;

public final class IllegalNumberOfPlayersException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void print(){
		System.out.println("The number of players is less than the index of player provided");
	}
}
