/**
 * 
 */
package snakesladders;

/**
 * @author Anuj_Varma
 *
 */
public final class Snakes extends BoardJump {
	protected void setNode1AndNode2(int _node1, int _node2){
		try{
			if(_node1 < _node2){
				throw new IllegalSnakeException();
			}
			super.setNode1AndNode2(_node1, _node2);
		}catch(IllegalSnakeException e){
			e.print();
		}
	}
}


