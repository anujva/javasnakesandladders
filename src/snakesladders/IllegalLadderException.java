/**
 * 
 */
package snakesladders;

/**
 * @author Anuj_Varma
 *
 */
public class IllegalLadderException extends Exception {
	private static final long serialVersionUID = -1242555669648468840L;
	
	void print(){
		System.out.println("Node1 value was less than Node2 value");
	}
}
