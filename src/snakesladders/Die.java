/**
 * 
 */
package snakesladders;

import java.util.Random;

/**
 * @author Anuj_Varma
 *
 */
public final class Die {
	private Random random;
	
	public Die(){
		random = new Random();
	}
	
	public int getRandomNumber(){
		 int r = (random.nextInt()%6 + 6) % 6;
		 return r+1;
	}
}
