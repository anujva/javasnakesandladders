package snakesladders;

public class Player {
	private String color;
	private int position;
	
	public Player(){
		color = "black";
		position = 1;
	}
	
	public Player(String _color){
		color = _color;
		position = 1;
	}
	
	public void setPlayerColor(String _color){
		color = _color;
	}
	
	public String getPlayerColor(){
		return color;
	}
	
	public void setPosition(int pos){
		position = pos;
	}
	
	public int getPosition(){
		return position;
	}
	
	public boolean hasWon(){
		if(position == 100){
			return true;
		}else{
			return false;
		}
	}
}
