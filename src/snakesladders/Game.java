/**
 * 
 */
package snakesladders;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Anuj_Varma
 *
 */
public final class Game {

	/**
	 * @param args
	 */
	
	private GameBoard gameBoard;
	private Player players[];
	private int numberOfPlayers;
	private Die die1;
	private Die die2;
	private final int maxPlayers = 7;
	private String playerColors[] = {"red", "blue", "green", "yellow", "black", "white", "orange", };
	private ArrayList<Integer> sortedPlayerOrder;
	
	public Game(int num_player) throws IllegalNumberOfPlayersException{
		if(num_player > maxPlayers){
			throw new IllegalNumberOfPlayersException();
		}
		gameBoard = new GameBoard();
		players = new Player[num_player];
		for(int i=0; i<num_player; i++){
			players[i] = new Player();
		}
		numberOfPlayers = num_player;
		for(int i=0; i<num_player; i++){
			setPlayerColor(i, playerColors[i]);
		}
		
		die1 = new Die();
		die2 = new Die();
        sortedPlayerOrder= new ArrayList<Integer>();
	}
	
	public void setPlayerColor(int player_num, String color) throws IllegalNumberOfPlayersException{
		if(player_num >= numberOfPlayers){
			throw new IllegalNumberOfPlayersException();
		}
		players[player_num].setPlayerColor(color);
	}
	
	public void choosePlayerOrder(){
		System.out.println("Welcome to Snakes And Ladders !!");
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" Each player will get a turn to throw the die, the player turn order will be according to the higher number thrown by each:");
		int playerThrownValue[] = new int[numberOfPlayers];
		
		for(int i=0; i<numberOfPlayers; i++){
			System.out.println("Player number "+ i + " throws the dice? <Say yes to throw >");
			try{
			    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			    String s = bufferRead.readLine();
			    if(s.matches("yes") || s.matches("YES") || s.matches("Yes") || s.matches("Y") || s.matches("y")){
			    	int random1 = die1.getRandomNumber();
			    	int random2 = die2.getRandomNumber();
			    	playerThrownValue[i] = random1 + random2;
			    	System.out.println("Player "+ i + " threw the number: " + playerThrownValue[i]);
			    }
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		
		int j=0;
		int largestthrownvalue=playerThrownValue[0];
		while(sortedPlayerOrder.size() != numberOfPlayers) {
			for (int i=0;i<numberOfPlayers;i++)
			{
				if (largestthrownvalue<playerThrownValue[i]){
					largestthrownvalue=playerThrownValue[i];
					j=i;	
				}
			}
			playerThrownValue[j]=0;
			sortedPlayerOrder.add(j);
			j=0;
			largestthrownvalue=playerThrownValue[0];
		}
	}
	
	public void printPlayerOrder(){
		for(int i=0; i<sortedPlayerOrder.size(); i++){
			System.out.println(sortedPlayerOrder.get(i));
		}
	}
	
	public void gameLoop(){
		boolean isGameFinished = false;
		int turn = 0;
		System.out.println("                                                                          ");
		System.out.println(" ");
		System.out.println("********************************************************************************************************");
		System.out.println("Lets begin the game : Player " + sortedPlayerOrder.get(turn) + "goes first");
		while(!isGameFinished){
			int playerIndex = sortedPlayerOrder.get(turn);
			System.out.println("This is Player " + playerIndex + "'s turn!");
			System.out.println("Thow the die? (yes)");
			int thrownValue = 0;
			try{
			    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			    String s = bufferRead.readLine();
			    if(s.matches("yes") || s.matches("YES") || s.matches("Yes") || s.matches("Y") || s.matches("y")){
			    	int random1 = die1.getRandomNumber();
			    	int random2 = die2.getRandomNumber();
			    	thrownValue = random1+random2;
			    }
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
			int position = players[playerIndex].getPosition() + thrownValue;
		
			boolean snakeFound = false;
			
			for(int i=0; i < gameBoard.snakes.length; i++){
				if(gameBoard.snakes[i].getNode1() == position){
					position = gameBoard.snakes[i].getNode2();
					snakeFound = true;
				}
			}
			
			boolean ladderfound = false;
			if(!snakeFound){
				for(int i=0; i < gameBoard.ladders.length; i++){
					if(gameBoard.ladders[i].getNode1() == position){
						position = gameBoard.ladders[i].getNode2();
						ladderfound = true;
					}
				}
			}else{
				System.out.println("Player "+ playerIndex+ " got bit by snake!!");
			}
			
			if(ladderfound){
				System.out.println("Player " + playerIndex + " found a ladder and climbed up!!");
			}
			
			players[playerIndex].setPosition(position);
			
			if(position >= 100){
				isGameFinished = true;
				System.out.println("Player "+ playerIndex + " won the game.. Thank you for playing");
			}else{
				turn++;
				turn = turn % numberOfPlayers;
				printGameState();
			}
		}
	}
	
	public void printGameState(){
		System.out.println("**************************************************************************************************************************************");
		System.out.println("The current position of Players: ");
		for(int i=0; i < numberOfPlayers; i++){
			System.out.println("Position of Player " + i + " is : " + players[i].getPosition());
		}
		System.out.println("**************************************************************************************************************************************");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Game game = new Game(2);
			game.choosePlayerOrder();
			game.gameLoop();
		} catch (IllegalNumberOfPlayersException e) {
			// TODO Auto-generated catch block
			e.print();
		}
		
	}
}

